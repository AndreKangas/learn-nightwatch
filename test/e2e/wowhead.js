var config = require('../../nightwatch.conf.js');

module.exports = {
   wowhead: function (browser) {
      browser
         .url('https://www.wowhead.com/')
         .waitForElementVisible('body', 2000)
         .windowSize('current', 1500, 1000)
         .getTitle(function(title) {
            this.assert.ok(title.includes("Wowhead:"));
          })
         .saveScreenshot(config.imgpath(browser) + 'wowhead_home.png')
         .assert.elementPresent('#onetrust-consent-sdk')
         .click('#onetrust-accept-btn-handler')
         .pause(500)
         .saveScreenshot(config.imgpath(browser) + 'cookies_accepted.png')
         .pause(500)
         .end();
         
   }
}